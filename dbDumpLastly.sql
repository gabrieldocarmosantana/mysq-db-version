-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: sgridd_sworker
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `__efmigrationshistory`
--

DROP TABLE IF EXISTS `__efmigrationshistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `__efmigrationshistory`
--

LOCK TABLES `__efmigrationshistory` WRITE;
/*!40000 ALTER TABLE `__efmigrationshistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `__efmigrationshistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `access_log`
--

DROP TABLE IF EXISTS `access_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `access_log` (
  `AccessLogId` varchar(128) NOT NULL,
  `UserId` longtext,
  `SessionId` longtext,
  `LoginDate` datetime(6) DEFAULT NULL,
  `LogoutDate` datetime(6) DEFAULT NULL,
  `IsAuthenticated` bit(1) NOT NULL,
  `IP` varchar(15) DEFAULT NULL,
  `Origin` int(11) NOT NULL,
  PRIMARY KEY (`AccessLogId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `access_log`
--

LOCK TABLES `access_log` WRITE;
/*!40000 ALTER TABLE `access_log` DISABLE KEYS */;
INSERT INTO `access_log` VALUES ('10af8ced-3fec-4ae8-bc67-7ab3f3f229a1','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','e70b66e9-a6f1-74a4-3b1a-c043aa2910e6','2021-02-09 09:15:01.533118','2021-02-09 09:18:27.888832',_binary '\0','201.69.124.166',1),('1b88edce-bc0e-4ea8-ac5d-7f9d61763643','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','e81595be-ed58-fa60-23dd-315375f9619e','2021-02-09 09:25:37.634852','2021-02-09 09:35:52.263590',_binary '\0','201.69.124.166',1),('216b1f8a-33fa-4b51-9485-dc1afabc258a','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','99b66e37-c6e6-d4d1-cfc0-895f34a27250','2021-02-09 09:36:33.344078','2021-02-09 09:51:33.344078',_binary '\0','201.69.124.166',1),('710798e0-a0dd-4e46-85d1-d3cafa111dc4','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','12069ba7-91b4-0ce5-0a23-1a8c2a18c82c','2021-02-09 09:36:09.006534','2021-02-09 09:36:27.480805',_binary '\0','201.69.124.166',1),('a1d40488-441f-49fd-a958-8c200e6ef83a','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','c58669ed-4967-c22b-a07f-e7a46f71f1a1','2021-02-09 09:10:45.650546','2021-02-09 09:14:48.178178',_binary '\0','201.69.124.166',1),('a25d3daa-2eff-469e-ba8a-9b62a3fe07e1','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','0753f493-0bc7-bea4-05a2-59c758cd9c1f','2021-02-09 21:32:15.354094','0001-01-01 00:00:00.000000',_binary '','191.183.197.190',1),('b4738b77-1dac-443a-9463-059ac3c88381','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','343432d6-acd1-8bf7-01ef-161e32b314be','2021-02-09 19:49:35.179903','2021-02-09 20:04:35.179903',_binary '\0','201.69.124.166',1),('bea3cee4-db2a-4033-8a9d-11556014ffe7','dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','78466c98-646d-58e2-a7fa-1465748a70d6','2021-02-09 21:03:52.925689','2021-02-09 21:18:52.925689',_binary '\0','191.183.197.190',1);
/*!40000 ALTER TABLE `access_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity`
--

DROP TABLE IF EXISTS `activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(30) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity`
--

LOCK TABLES `activity` WRITE;
/*!40000 ALTER TABLE `activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activity_task`
--

DROP TABLE IF EXISTS `activity_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activity_task` (
  `ActivityId` int(11) NOT NULL,
  `TaskId` int(11) NOT NULL,
  `Sequence` int(11) DEFAULT '0',
  `Observation` text,
  PRIMARY KEY (`ActivityId`,`TaskId`),
  KEY `activity_task_ibfk_2` (`TaskId`),
  CONSTRAINT `activity_task_ibfk_1` FOREIGN KEY (`ActivityId`) REFERENCES `activity` (`Id`),
  CONSTRAINT `activity_task_ibfk_2` FOREIGN KEY (`TaskId`) REFERENCES `task` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activity_task`
--

LOCK TABLES `activity_task` WRITE;
/*!40000 ALTER TABLE `activity_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `activity_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `CompanyUnityId` int(11) NOT NULL AUTO_INCREMENT,
  `Street` varchar(100) DEFAULT NULL,
  `NeighborHood` varchar(30) DEFAULT NULL,
  `State` varchar(30) DEFAULT NULL,
  `Complement` varchar(30) DEFAULT NULL,
  `Number` longtext,
  `ZipCode` varchar(30) DEFAULT NULL,
  `City` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`CompanyUnityId`),
  CONSTRAINT `FK_address_company_unity_CompanyUnityId` FOREIGN KEY (`CompanyUnityId`) REFERENCES `company_unity` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1001,'Rua Gomes de Carvalho','Vila Ilimpia','SP','9o Andar','1996','04547006','São Paulo'),(1002,'Rua Florentino Faller','Enseada do Suá','ES','Ed Maxxi I','80','29050310','Vitória');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinical_history`
--

DROP TABLE IF EXISTS `clinical_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinical_history` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProfessionalId` int(11) NOT NULL DEFAULT '0',
  `DateOfMeasurement` datetime NOT NULL,
  `SkinTemperatureMin` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SkinTemperatureAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SkinTemperatureMax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `RespiratoryActivityMin` decimal(10,2) NOT NULL DEFAULT '0.00',
  `RespiratoryActivityAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `RespiratoryActivityMax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SisBloodPressureMin` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SisBloodPressureAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `SisBloodPressureMax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `DiasBloodPressureMin` decimal(10,2) NOT NULL DEFAULT '0.00',
  `DiasBloodPressureAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `DiasBloodPressureMax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `HeartRateMin` decimal(10,2) NOT NULL DEFAULT '0.00',
  `HeartRateAvg` decimal(10,2) NOT NULL DEFAULT '0.00',
  `HeartRateMax` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`Id`,`DateOfMeasurement`),
  KEY `fk_clinical_parameter_clinical_history_idx` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinical_history`
--

LOCK TABLES `clinical_history` WRITE;
/*!40000 ALTER TABLE `clinical_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinical_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clinical_parameter`
--

DROP TABLE IF EXISTS `clinical_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clinical_parameter` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProfessionalId` int(11) NOT NULL,
  `SkinTemperatureMin` decimal(10,2) NOT NULL,
  `SkinTemperatureMax` decimal(10,2) NOT NULL,
  `HeartRateMin` decimal(10,2) NOT NULL,
  `HeartRateMax` decimal(10,2) NOT NULL,
  `SisBloodPressureMin` decimal(10,2) NOT NULL,
  `SisBloodPressureMax` decimal(10,2) NOT NULL,
  `DiasBloodPressureMin` decimal(10,2) NOT NULL,
  `DiasBloodPressureMax` decimal(10,2) NOT NULL,
  `BloodType` varchar(3) DEFAULT NULL COMMENT 'Tipo Sangu�neo do Profissional (A+, B+, O+, AB+, A-, B-, O-, AB-)',
  PRIMARY KEY (`Id`),
  KEY `ProfessionalId` (`ProfessionalId`),
  CONSTRAINT `clinical_parameter_ibfk_1` FOREIGN KEY (`ProfessionalId`) REFERENCES `professional` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clinical_parameter`
--

LOCK TABLES `clinical_parameter` WRITE;
/*!40000 ALTER TABLE `clinical_parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `clinical_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Cnpj` varchar(18) DEFAULT NULL,
  `Responsible` varchar(50) DEFAULT NULL,
  `Phone` longtext,
  `Email` varchar(100) DEFAULT NULL,
  `Active` bit(1) NOT NULL DEFAULT b'0',
  `LogoPath` varchar(256) DEFAULT NULL,
  `Settings` longtext,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
INSERT INTO `company` VALUES (1,'EDP BR','22175259000106','Administrador EDP BR','1133709990','admin@edpbr.com.br',_binary '\0','files/090220210914045620060820201140284807edp-logo.png','{ \"zoom\":{\"ApiKey\":\"nBhA0UjlQJqfSwlZd9u7uQ\", \"ApiSecret\":\"DGswdcThpKubnZh2rY8Xlmpgsab2yzOWZqnV\",\"userId\":\"tfAZvoZLSpy3010bVQiMLA\"},\"sbox\" : { \"user\": \"edpbr\", \"password\" : \"#3dP3r@5b0X#@dm1n\" }}');
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_unity`
--

DROP TABLE IF EXISTS `company_unity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_unity` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyId` int(11) NOT NULL,
  `Phone` bigint(20) NOT NULL,
  `Active` bit(1) NOT NULL DEFAULT b'0',
  `Name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `IX_company_unity_CompanyId` (`CompanyId`),
  CONSTRAINT `FK_company_unity_company_CompanyId` FOREIGN KEY (`CompanyId`) REFERENCES `company` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1003 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_unity`
--

LOCK TABLES `company_unity` WRITE;
/*!40000 ALTER TABLE `company_unity` DISABLE KEYS */;
INSERT INTO `company_unity` VALUES (1001,1,1133709990,_binary '','EDP São Paulo'),(1002,1,11337099900,_binary '','EDP Espírito Santo');
/*!40000 ALTER TABLE `company_unity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company_unity_professional`
--

DROP TABLE IF EXISTS `company_unity_professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company_unity_professional` (
  `CompanyUnityId` int(11) NOT NULL,
  `ProfessionalId` int(11) NOT NULL,
  `StartDate` datetime(6) NOT NULL,
  `EndtDate` datetime(6) NOT NULL,
  PRIMARY KEY (`CompanyUnityId`,`ProfessionalId`),
  KEY `IX_company_unity_professional_ProfessionalId` (`ProfessionalId`),
  CONSTRAINT `FK_company_unity_professional_company_unity_CompanyUnityId` FOREIGN KEY (`CompanyUnityId`) REFERENCES `company_unity` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company_unity_professional`
--

LOCK TABLES `company_unity_professional` WRITE;
/*!40000 ALTER TABLE `company_unity_professional` DISABLE KEYS */;
INSERT INTO `company_unity_professional` VALUES (1001,1,'2020-08-05 13:09:35.000000','0001-01-01 00:00:00.000000');
/*!40000 ALTER TABLE `company_unity_professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `context`
--

DROP TABLE IF EXISTS `context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `context` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` int(11) DEFAULT NULL,
  `Name` varchar(150) DEFAULT NULL,
  `Application` smallint(6) DEFAULT NULL,
  `Parameters` text,
  `Type` smallint(6) NOT NULL,
  `CharacterizationId` int(11) DEFAULT NULL,
  `IntervalTime` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `id_fk_caracterization` (`CharacterizationId`),
  CONSTRAINT `id_fk_caracterization` FOREIGN KEY (`CharacterizationId`) REFERENCES `occurrence_characterization` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `context`
--

LOCK TABLES `context` WRITE;
/*!40000 ALTER TABLE `context` DISABLE KEYS */;
/*!40000 ALTER TABLE `context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(100) DEFAULT NULL,
  `Cnpj` bigint(20) NOT NULL,
  `RegisterNumber` varchar(100) DEFAULT NULL,
  `Phone` varchar(100) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'Interno',0,'1','0','',1);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipament`
--

DROP TABLE IF EXISTS `equipament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipament` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` varchar(50) NOT NULL,
  `Hwid` varchar(50) NOT NULL,
  `ClassificationId` int(11) NOT NULL,
  `Manufacturer` varchar(30) NOT NULL,
  `Brand` varchar(30) NOT NULL,
  `Model` varchar(30) NOT NULL,
  `Release` varchar(10) DEFAULT NULL,
  `ManufactureDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` smallint(6) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `ClassificationId` (`ClassificationId`),
  CONSTRAINT `equipament_ibfk_1` FOREIGN KEY (`ClassificationId`) REFERENCES `equipament_classification` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipament`
--

LOCK TABLES `equipament` WRITE;
/*!40000 ALTER TABLE `equipament` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipament_classification`
--

DROP TABLE IF EXISTS `equipament_classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipament_classification` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TypeId` int(11) NOT NULL,
  `Code` varchar(10) NOT NULL,
  `Description` varchar(50) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TypeId` (`TypeId`),
  CONSTRAINT `equipament_classification_ibfk_1` FOREIGN KEY (`TypeId`) REFERENCES `equipament_type` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipament_classification`
--

LOCK TABLES `equipament_classification` WRITE;
/*!40000 ALTER TABLE `equipament_classification` DISABLE KEYS */;
INSERT INTO `equipament_classification` VALUES (1,1,'1001','Bota tipo A',NULL),(2,1,'1002','Luva tipo A',NULL),(3,1,'1003','Capacete tipo A',NULL),(4,1,'1004','Uniforme tipo A',NULL),(5,2,'2001','Cone',NULL),(6,3,'3001','Escada',NULL),(7,4,'4001','Detector de Potencial',NULL);
/*!40000 ALTER TABLE `equipament_classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipament_inspection`
--

DROP TABLE IF EXISTS `equipament_inspection`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipament_inspection` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `EquipamentId` int(11) NOT NULL,
  `InspectionDate` datetime NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `ResponsibleId` int(11) NOT NULL,
  `Observation` text,
  PRIMARY KEY (`Id`),
  KEY `EquipamentId` (`EquipamentId`),
  KEY `ResponsibleId` (`ResponsibleId`),
  CONSTRAINT `equipament_inspection_ibfk_1` FOREIGN KEY (`EquipamentId`) REFERENCES `equipament` (`Id`),
  CONSTRAINT `equipament_inspection_ibfk_2` FOREIGN KEY (`ResponsibleId`) REFERENCES `professional` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipament_inspection`
--

LOCK TABLES `equipament_inspection` WRITE;
/*!40000 ALTER TABLE `equipament_inspection` DISABLE KEYS */;
/*!40000 ALTER TABLE `equipament_inspection` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipament_type`
--

DROP TABLE IF EXISTS `equipament_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipament_type` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(10) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipament_type`
--

LOCK TABLES `equipament_type` WRITE;
/*!40000 ALTER TABLE `equipament_type` DISABLE KEYS */;
INSERT INTO `equipament_type` VALUES (1,'01','EPI'),(2,'02','EPC'),(3,'03','Ferramenta'),(4,'04','Equipamento');
/*!40000 ALTER TABLE `equipament_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occurrence`
--

DROP TABLE IF EXISTS `occurrence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occurrence` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Number` int(11) DEFAULT NULL,
  `Origin` smallint(6) NOT NULL,
  `Type` smallint(6) NOT NULL,
  `ProfessionalId` int(11) NOT NULL,
  `CharacterizationId` int(11) NOT NULL,
  `ServiceOrderId` int(11) DEFAULT NULL,
  `ContextId` int(11) DEFAULT NULL,
  `RegisterDate` datetime DEFAULT '0000-00-00 00:00:00',
  `Local` varchar(100) DEFAULT NULL,
  `Description` text,
  `Observation` text,
  `Evaluated` tinyint(1) DEFAULT NULL,
  `Acknowledged` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occurrence`
--

LOCK TABLES `occurrence` WRITE;
/*!40000 ALTER TABLE `occurrence` DISABLE KEYS */;
/*!40000 ALTER TABLE `occurrence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occurrence_characterization`
--

DROP TABLE IF EXISTS `occurrence_characterization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occurrence_characterization` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` smallint(6) NOT NULL,
  `Description` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occurrence_characterization`
--

LOCK TABLES `occurrence_characterization` WRITE;
/*!40000 ALTER TABLE `occurrence_characterization` DISABLE KEYS */;
/*!40000 ALTER TABLE `occurrence_characterization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occurrence_classification`
--

DROP TABLE IF EXISTS `occurrence_classification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occurrence_classification` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Type` smallint(6) NOT NULL,
  `Description` varchar(150) DEFAULT NULL,
  `Severity` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occurrence_classification`
--

LOCK TABLES `occurrence_classification` WRITE;
/*!40000 ALTER TABLE `occurrence_classification` DISABLE KEYS */;
INSERT INTO `occurrence_classification` VALUES (1,1,'Ato inseguro',1),(2,1,'Quase acidente',2),(3,1,'Acidente sem afastamento',3),(4,1,'Acidente com afastamento',4),(5,1,'Fatalidade',5),(6,2,'LEVE',1),(7,2,'MÉDIA',2),(8,2,'GRAVE',3),(9,2,'GRAVÍSSIMA',4),(10,3,'Informativo',1),(11,3,'Mobilização',2),(12,3,'Atenção',3),(13,3,'Alerta',4),(14,3,'Crítico',5);
/*!40000 ALTER TABLE `occurrence_classification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occurrence_file`
--

DROP TABLE IF EXISTS `occurrence_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occurrence_file` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico da Ocorr�ncia do Arquivo',
  `OccurrenceId` int(11) NOT NULL COMMENT 'Ocorr�ncia ao qual o Arquivo est� relacionado',
  `FileType` int(1) NOT NULL COMMENT 'Tipo de Arquivo (1 = V�deo ou 2 = Imagem)',
  `Description` varchar(100) DEFAULT NULL COMMENT 'Descri��o do momento capturado no Arquivo (opcional)',
  `FileAddress` text NOT NULL COMMENT 'Endere�o f�sico do arquivo da Ocorr�ncia',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occurrence_file`
--

LOCK TABLES `occurrence_file` WRITE;
/*!40000 ALTER TABLE `occurrence_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `occurrence_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `occurrence_recognition`
--

DROP TABLE IF EXISTS `occurrence_recognition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `occurrence_recognition` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `OccurrenceId` int(11) NOT NULL,
  `SupervisorId` int(11) NOT NULL,
  `ProfessionalId` int(11) NOT NULL,
  `CharacterizationId` int(11) NOT NULL,
  `ClassificationId` int(11) NOT NULL,
  `RegisterDate` datetime DEFAULT NULL,
  `Local` varchar(100) DEFAULT NULL,
  `Description` text,
  `Observation` text,
  `DaysWorkedAfterDayOff` int(11) DEFAULT NULL,
  `AbsenceDays` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `OccurrenceId` (`OccurrenceId`),
  KEY `ProfessionalId` (`ProfessionalId`),
  KEY `CharacterizationId` (`CharacterizationId`),
  KEY `ClassificationId` (`ClassificationId`),
  CONSTRAINT `occurrence_recognition_ibfk_1` FOREIGN KEY (`OccurrenceId`) REFERENCES `occurrence` (`Id`),
  CONSTRAINT `occurrence_recognition_ibfk_2` FOREIGN KEY (`ProfessionalId`) REFERENCES `professional` (`Id`),
  CONSTRAINT `occurrence_recognition_ibfk_3` FOREIGN KEY (`CharacterizationId`) REFERENCES `occurrence_characterization` (`Id`),
  CONSTRAINT `occurrence_recognition_ibfk_4` FOREIGN KEY (`ClassificationId`) REFERENCES `occurrence_classification` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `occurrence_recognition`
--

LOCK TABLES `occurrence_recognition` WRITE;
/*!40000 ALTER TABLE `occurrence_recognition` DISABLE KEYS */;
/*!40000 ALTER TABLE `occurrence_recognition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodic_exam`
--

DROP TABLE IF EXISTS `periodic_exam`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodic_exam` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ProfessionalId` int(11) NOT NULL,
  `ExamDate` datetime NOT NULL,
  `ExpirationDate` datetime NOT NULL,
  `Age` smallint(6) NOT NULL,
  `Height` decimal(10,2) DEFAULT NULL,
  `Weight` decimal(10,2) DEFAULT NULL,
  `Waist` decimal(10,2) DEFAULT NULL,
  `IMC` decimal(10,2) DEFAULT NULL,
  `Observation` text,
  PRIMARY KEY (`Id`),
  KEY `ProfessionalId` (`ProfessionalId`),
  CONSTRAINT `periodic_exam_ibfk_1` FOREIGN KEY (`ProfessionalId`) REFERENCES `professional` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodic_exam`
--

LOCK TABLES `periodic_exam` WRITE;
/*!40000 ALTER TABLE `periodic_exam` DISABLE KEYS */;
/*!40000 ALTER TABLE `periodic_exam` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional`
--

DROP TABLE IF EXISTS `professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional` (
  `UserId` char(36) NOT NULL,
  `CompanyUnityId` int(11) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Cpf` bigint(20) NOT NULL,
  `RegisterNumber` longtext,
  `Phone` bigint(20) NOT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `StandardSupervisor` int(1) DEFAULT NULL,
  `ProfessionalEquipamentEquipamentId` int(11) DEFAULT NULL,
  `ProfessionalEquipamentProfessionalId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional`
--

LOCK TABLES `professional` WRITE;
/*!40000 ALTER TABLE `professional` DISABLE KEYS */;
INSERT INTO `professional` VALUES ('dbc4704d-7e3b-4219-9e61-d6f7d7d465a6',1001,'Administrador EDP BR',11111111111,NULL,1111111111,'admin@edpbr.com.br',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professional_equipament`
--

DROP TABLE IF EXISTS `professional_equipament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professional_equipament` (
  `ProfessionalId` int(11) NOT NULL,
  `EquipamentId` int(11) NOT NULL,
  `assignmentdate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `returndate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  PRIMARY KEY (`ProfessionalId`,`EquipamentId`),
  KEY `EquipamentId` (`EquipamentId`),
  CONSTRAINT `professional_equipament_ibfk_1` FOREIGN KEY (`ProfessionalId`) REFERENCES `professional` (`Id`),
  CONSTRAINT `professional_equipament_ibfk_2` FOREIGN KEY (`EquipamentId`) REFERENCES `equipament` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professional_equipament`
--

LOCK TABLES `professional_equipament` WRITE;
/*!40000 ALTER TABLE `professional_equipament` DISABLE KEYS */;
/*!40000 ALTER TABLE `professional_equipament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `Id` varchar(128) NOT NULL,
  `Name` varchar(256) DEFAULT NULL,
  `NormalizedName` varchar(128) DEFAULT NULL,
  `ConcurrencyStamp` longtext,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `RoleNameIndex` (`NormalizedName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES ('473694e0-2321-411a-803e-3556e866fb39','Administrador','ADMINISTRADOR','394daedf-6a9e-439b-b6c5-afb00484631d');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_claim`
--

DROP TABLE IF EXISTS `role_claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_claim` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `RoleId` varchar(128) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`Id`),
  KEY `IX_Role_Claim_RoleId` (`RoleId`),
  CONSTRAINT `FK_Role_Claim_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `role` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_claim`
--

LOCK TABLES `role_claim` WRITE;
/*!40000 ALTER TABLE `role_claim` DISABLE KEYS */;
INSERT INTO `role_claim` VALUES (7,'473694e0-2321-411a-803e-3556e866fb39','administration','4'),(8,'473694e0-2321-411a-803e-3556e866fb39','services','4'),(9,'473694e0-2321-411a-803e-3556e866fb39','safety','4'),(10,'473694e0-2321-411a-803e-3556e866fb39','medicine','4'),(11,'473694e0-2321-411a-803e-3556e866fb39','operation','4'),(12,'473694e0-2321-411a-803e-3556e866fb39','audit','4'),(13,'473694e0-2321-411a-803e-3556e866fb39','monitoring','4');
/*!40000 ALTER TABLE `role_claim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order`
--

DROP TABLE IF EXISTS `service_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(15) NOT NULL,
  `Description` varchar(80) NOT NULL,
  `Street` varchar(128) DEFAULT NULL,
  `Number` varchar(64) DEFAULT NULL,
  `Complement` varchar(64) DEFAULT NULL,
  `Neighborhood` varchar(128) DEFAULT NULL,
  `City` varchar(128) DEFAULT NULL,
  `State` varchar(64) DEFAULT NULL,
  `ZipCode` char(9) DEFAULT NULL,
  `StatusId` int(11) NOT NULL,
  `RequestDate` datetime DEFAULT NULL,
  `ExecutionDate` datetime DEFAULT NULL,
  `CheckIn` smallint(6) DEFAULT NULL,
  `CheckOut` smallint(6) DEFAULT NULL,
  `CheckInDate` datetime DEFAULT NULL,
  `CheckOutDate` datetime DEFAULT NULL,
  `Dispatch` smallint(6) DEFAULT NULL,
  `SupervisorId` int(11) NOT NULL,
  `IsActive` tinyint(1) DEFAULT NULL,
  `TagId` int(11) DEFAULT NULL COMMENT 'Associa��o de qual Ponto que gerou a Ordem de Servi�o',
  `CustomerId` int(11) DEFAULT '1' COMMENT 'Associação para qual cliente o serço será prestado',
  `MeetingId` varchar(50) DEFAULT NULL,
  `MeetingStatus` int(1) DEFAULT '1',
  PRIMARY KEY (`Id`),
  KEY `StatusId` (`StatusId`),
  KEY `SupervisorId` (`SupervisorId`),
  CONSTRAINT `service_order_ibfk_1` FOREIGN KEY (`StatusId`) REFERENCES `service_order_status` (`Id`),
  CONSTRAINT `service_order_ibfk_2` FOREIGN KEY (`SupervisorId`) REFERENCES `professional` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order`
--

LOCK TABLES `service_order` WRITE;
/*!40000 ALTER TABLE `service_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_file`
--

DROP TABLE IF EXISTS `service_order_file`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_file` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `OrderServiceId` int(11) NOT NULL,
  `ProfessionalId` int(11) NOT NULL,
  `Path` text NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `OrderServiceId` (`OrderServiceId`),
  CONSTRAINT `service_order_file_ibfk_1` FOREIGN KEY (`OrderServiceId`) REFERENCES `service_order` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_file`
--

LOCK TABLES `service_order_file` WRITE;
/*!40000 ALTER TABLE `service_order_file` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_order_file` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_professional`
--

DROP TABLE IF EXISTS `service_order_professional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_professional` (
  `ServiceOrderId` int(11) NOT NULL DEFAULT '0',
  `ProfessionalId` int(11) NOT NULL,
  `IsResponsible` tinyint(1) DEFAULT NULL,
  `Accept` tinyint(1) DEFAULT NULL,
  `Reason` varchar(50) DEFAULT NULL,
  `CheckinDate` datetime DEFAULT NULL COMMENT 'Data de Check-in (in�cio) de execu��o da solicita��o de servi�o',
  `CheckoutDate` datetime DEFAULT NULL COMMENT 'Data de Check-out (fim) da execu��o da solicita��o de servi�o',
  PRIMARY KEY (`ServiceOrderId`,`ProfessionalId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_professional`
--

LOCK TABLES `service_order_professional` WRITE;
/*!40000 ALTER TABLE `service_order_professional` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_order_professional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_status`
--

DROP TABLE IF EXISTS `service_order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_status` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(15) DEFAULT NULL,
  `Sequence` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_status`
--

LOCK TABLES `service_order_status` WRITE;
/*!40000 ALTER TABLE `service_order_status` DISABLE KEYS */;
INSERT INTO `service_order_status` VALUES (1,'Pendente',1),(2,'Executando',2),(3,'Revisão',3),(4,'Cancelado',4),(5,'Concluído',5);
/*!40000 ALTER TABLE `service_order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_task`
--

DROP TABLE IF EXISTS `service_order_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_task` (
  `ServiceOrderId` int(11) NOT NULL DEFAULT '0',
  `TaskId` int(11) NOT NULL,
  `Planned` tinyint(1) DEFAULT NULL,
  `Executed` tinyint(1) DEFAULT NULL,
  `Observation` text,
  `Sequence` int(11) DEFAULT '1' COMMENT 'Profissional que escolheu executar a Tarefa da OS',
  `ProfessionalId` int(11) DEFAULT NULL,
  `CheckinDate` datetime DEFAULT NULL,
  KEY `TaskId` (`TaskId`),
  KEY `service_order_task_ibfk_1` (`ServiceOrderId`),
  CONSTRAINT `service_order_task_ibfk_1` FOREIGN KEY (`ServiceOrderId`) REFERENCES `service_order` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_task`
--

LOCK TABLES `service_order_task` WRITE;
/*!40000 ALTER TABLE `service_order_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_order_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_order_task_step`
--

DROP TABLE IF EXISTS `service_order_task_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_order_task_step` (
  `ServiceOrderId` int(11) NOT NULL DEFAULT '0',
  `TaskId` int(11) NOT NULL,
  `TaskStepId` int(11) NOT NULL,
  `ProfessionalId` int(11) DEFAULT NULL,
  `ExecutionDate` datetime NOT NULL,
  `Result` varchar(100) DEFAULT NULL,
  `EvidenceFile` varchar(500) DEFAULT NULL,
  `EvidenceRegistryType` smallint(6) DEFAULT NULL,
  `EvidenceRegistry` blob,
  PRIMARY KEY (`ServiceOrderId`,`TaskId`,`TaskStepId`),
  KEY `TaskId` (`TaskId`),
  KEY `TaskStepId` (`TaskStepId`),
  CONSTRAINT `service_order_task_step_ibfk_1` FOREIGN KEY (`ServiceOrderId`) REFERENCES `service_order` (`Id`),
  CONSTRAINT `service_order_task_step_ibfk_2` FOREIGN KEY (`TaskId`) REFERENCES `task` (`Id`),
  CONSTRAINT `service_order_task_step_ibfk_3` FOREIGN KEY (`TaskStepId`) REFERENCES `task_step` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_order_task_step`
--

LOCK TABLES `service_order_task_step` WRITE;
/*!40000 ALTER TABLE `service_order_task_step` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_order_task_step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Uid` varchar(50) NOT NULL,
  `Hwid` varchar(50) NOT NULL,
  `RegistrationDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` smallint(6) NOT NULL,
  `Localization` varchar(300) DEFAULT NULL,
  `Mode` int(11) DEFAULT '1',
  `TagTypeId` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `tag_ibfk_1` (`TagTypeId`),
  CONSTRAINT `tag_ibfk_1` FOREIGN KEY (`TagTypeId`) REFERENCES `tag_type` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag`
--

LOCK TABLES `tag` WRITE;
/*!40000 ALTER TABLE `tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_task`
--

DROP TABLE IF EXISTS `tag_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_task` (
  `TagId` int(11) NOT NULL,
  `TaskId` int(11) NOT NULL,
  `Sequence` int(11) DEFAULT '1',
  PRIMARY KEY (`TagId`,`TaskId`),
  KEY `tag_task_ibfk_2` (`TaskId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_task`
--

LOCK TABLES `tag_task` WRITE;
/*!40000 ALTER TABLE `tag_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tag_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tag_type`
--

DROP TABLE IF EXISTS `tag_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag_type` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tag_type`
--

LOCK TABLES `tag_type` WRITE;
/*!40000 ALTER TABLE `tag_type` DISABLE KEYS */;
INSERT INTO `tag_type` VALUES (1,'Tarefa'),(2,'Check-Point');
/*!40000 ALTER TABLE `tag_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TaskGroupId` int(11) NOT NULL,
  `Code` varchar(30) NOT NULL,
  `Name` varchar(300) DEFAULT NULL,
  `ProfessionalsCount` int(11) DEFAULT NULL,
  `EstimatedTime` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TaskGroupId` (`TaskGroupId`),
  CONSTRAINT `task_ibfk_1` FOREIGN KEY (`TaskGroupId`) REFERENCES `task_group` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_equipament`
--

DROP TABLE IF EXISTS `task_equipament`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_equipament` (
  `TaskId` int(11) NOT NULL,
  `EquipamentId` int(11) NOT NULL,
  PRIMARY KEY (`TaskId`,`EquipamentId`),
  KEY `ResourceId` (`EquipamentId`),
  CONSTRAINT `task_equipament_ibfk_1` FOREIGN KEY (`TaskId`) REFERENCES `task` (`Id`),
  CONSTRAINT `task_equipament_ibfk_2` FOREIGN KEY (`EquipamentId`) REFERENCES `equipament_classification` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_equipament`
--

LOCK TABLES `task_equipament` WRITE;
/*!40000 ALTER TABLE `task_equipament` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_equipament` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_group`
--

DROP TABLE IF EXISTS `task_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_group` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Code` varchar(30) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Version` varchar(10) DEFAULT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_group`
--

LOCK TABLES `task_group` WRITE;
/*!40000 ALTER TABLE `task_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_resource`
--

DROP TABLE IF EXISTS `task_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_resource` (
  `TaskId` int(11) NOT NULL,
  `ResourceId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`TaskId`,`ResourceId`),
  KEY `ResourceId` (`ResourceId`),
  CONSTRAINT `task_resource_ibfk_1` FOREIGN KEY (`TaskId`) REFERENCES `task` (`Id`),
  CONSTRAINT `task_resource_ibfk_2` FOREIGN KEY (`ResourceId`) REFERENCES `equipament_classification` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_resource`
--

LOCK TABLES `task_resource` WRITE;
/*!40000 ALTER TABLE `task_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_step`
--

DROP TABLE IF EXISTS `task_step`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_step` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `TaskId` int(11) NOT NULL,
  `Number` varchar(5) NOT NULL,
  `Description` varchar(100) NOT NULL,
  `TaskTypeId` int(11) NOT NULL,
  `TaskTypeComplement` varchar(100) DEFAULT NULL,
  `Development` text NOT NULL,
  `Risks` text,
  `Control` text,
  `Notes` text,
  `SubTaskId` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `TaskId` (`TaskId`),
  KEY `SubTaskId` (`SubTaskId`),
  KEY `TaskTypeId` (`TaskTypeId`),
  CONSTRAINT `task_step_ibfk_1` FOREIGN KEY (`TaskId`) REFERENCES `task` (`Id`),
  CONSTRAINT `task_step_ibfk_2` FOREIGN KEY (`SubTaskId`) REFERENCES `task` (`Id`),
  CONSTRAINT `task_step_ibfk_3` FOREIGN KEY (`TaskTypeId`) REFERENCES `task_type` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_step`
--

LOCK TABLES `task_step` WRITE;
/*!40000 ALTER TABLE `task_step` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_step` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_step_context`
--

DROP TABLE IF EXISTS `task_step_context`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_step_context` (
  `TaskStepId` int(11) NOT NULL,
  `ContextId` int(11) NOT NULL,
  `StandbyTime` int(11) DEFAULT NULL,
  PRIMARY KEY (`TaskStepId`,`ContextId`),
  KEY `ContextId` (`ContextId`),
  CONSTRAINT `task_step_context_ibfk_1` FOREIGN KEY (`TaskStepId`) REFERENCES `task_step` (`Id`),
  CONSTRAINT `task_step_context_ibfk_2` FOREIGN KEY (`ContextId`) REFERENCES `context` (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_step_context`
--

LOCK TABLES `task_step_context` WRITE;
/*!40000 ALTER TABLE `task_step_context` DISABLE KEYS */;
/*!40000 ALTER TABLE `task_step_context` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_type`
--

DROP TABLE IF EXISTS `task_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `task_type` (
  `Id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identificador �nico do Tipo de Tarefa',
  `Description` varchar(50) NOT NULL COMMENT 'Descri��o do Tipo de Tarefa (Ex.: Sensor, Instru��o, Ponto, Registro de Foto, Quest�o, Manuscrito, Check-Point)',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_type`
--

LOCK TABLES `task_type` WRITE;
/*!40000 ALTER TABLE `task_type` DISABLE KEYS */;
INSERT INTO `task_type` VALUES (1,'Sensor'),(2,'Instrução'),(3,'Ponto'),(4,'Registro de Foto'),(5,'Questão'),(6,'Manuscrito'),(7,'Referência de Tarefa'),(8,'Check-Point');
/*!40000 ALTER TABLE `task_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `Id` varchar(128) NOT NULL,
  `UserName` varchar(256) DEFAULT NULL,
  `NormalizedUserName` varchar(128) DEFAULT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `NormalizedEmail` varchar(128) DEFAULT NULL,
  `EmailConfirmed` bit(1) NOT NULL,
  `PasswordHash` longtext,
  `SecurityStamp` longtext,
  `ConcurrencyStamp` longtext,
  `PhoneNumber` longtext,
  `PhoneNumberConfirmed` bit(1) NOT NULL,
  `TwoFactorEnabled` bit(1) NOT NULL,
  `LockoutEnd` datetime(6) DEFAULT NULL,
  `LockoutEnabled` bit(1) NOT NULL,
  `AccessFailedCount` int(11) NOT NULL,
  `Name` longtext,
  `LockedOutReason` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `UserNameIndex` (`NormalizedUserName`),
  KEY `EmailIndex` (`NormalizedEmail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','admin@edpbr.com.br','ADMIN@EDPBR.COM.BR','admin@edpbr.com.br','ADMIN@EDPBR.COM.BR',_binary '','AQAAAAEAACcQAAAAEDzSfW6Qtyu5aJaAdGLUbZqp4yGPWFqdXitw3E9Brj9PWGKR7fNKWXLL4ey+WjnSXw==','GPLPEIPTAP5FMXQOBSWOVIYX3T2UK743','aac31d96-eb38-446f-82a2-fec96df75326','1133709990',_binary '',_binary '\0',NULL,_binary '',0,'Administrador EDP BR',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_claim`
--

DROP TABLE IF EXISTS `user_claim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_claim` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `UserId` varchar(128) NOT NULL,
  `ClaimType` longtext,
  `ClaimValue` longtext,
  PRIMARY KEY (`Id`),
  KEY `IX_User_Claim_UserId` (`UserId`),
  CONSTRAINT `FK_User_Claim_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_claim`
--

LOCK TABLES `user_claim` WRITE;
/*!40000 ALTER TABLE `user_claim` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_claim` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_login`
--

DROP TABLE IF EXISTS `user_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_login` (
  `LoginProvider` varchar(255) NOT NULL,
  `ProviderKey` varchar(128) NOT NULL,
  `ProviderDisplayName` longtext,
  `UserId` varchar(128) NOT NULL,
  PRIMARY KEY (`LoginProvider`,`ProviderKey`),
  KEY `IX_User_Login_UserId` (`UserId`),
  CONSTRAINT `FK_User_Login_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_login`
--

LOCK TABLES `user_login` WRITE;
/*!40000 ALTER TABLE `user_login` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `UserId` varchar(128) NOT NULL,
  `RoleId` varchar(128) NOT NULL,
  PRIMARY KEY (`UserId`,`RoleId`),
  KEY `IX_User_Role_RoleId` (`RoleId`),
  CONSTRAINT `FK_User_Role_Role_RoleId` FOREIGN KEY (`RoleId`) REFERENCES `role` (`Id`) ON DELETE CASCADE,
  CONSTRAINT `FK_User_Role_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES ('dbc4704d-7e3b-4219-9e61-d6f7d7d465a6','473694e0-2321-411a-803e-3556e866fb39');
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_token`
--

DROP TABLE IF EXISTS `user_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_token` (
  `UserId` varchar(128) NOT NULL,
  `LoginProvider` varchar(128) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Value` longtext,
  PRIMARY KEY (`UserId`,`LoginProvider`,`Name`),
  CONSTRAINT `FK_User_Token_User_UserId` FOREIGN KEY (`UserId`) REFERENCES `user` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_token`
--

LOCK TABLES `user_token` WRITE;
/*!40000 ALTER TABLE `user_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_token` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-02-10 17:41:15
